package com.jazzteam.martynchyk.service;

import com.jazzteam.martynchyk.entity.robots.Robot;
import com.jazzteam.martynchyk.entity.robots.implemetation.RobotSolder;
import com.jazzteam.martynchyk.entity.task.Task;
import com.jazzteam.martynchyk.entity.task.TaskStatus;
import com.jazzteam.martynchyk.entity.task.TaskType;
import com.jazzteam.martynchyk.factory.RobotsFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProcessorService {

    private List<Task> tasks = new ArrayList<>();
    private List<Robot> robots = new ArrayList();

    public void execute() {
        Task task;
        Robot robot;
        initializeData();
        while (true) {
            task = findNewTask();
            if (task == null) {
                System.out.println("No actual tasks");
                continue;
            }
            task.setStatus(TaskStatus.INPROCESS);
            robot = findRobotByTaskType(task.getType());
            if (robot == null) {
                System.out.println("Robot not found for task " + task.getId().toString());
                task.setStatus(TaskStatus.CREATED);
                robots.add(RobotsFactory.CreteRobot(task.getType()));
                continue;
            }
            if (task.getType() == TaskType.KILLYOURSELF) {
                robots.remove(robot);
                task.setStatus(TaskStatus.DONE);
                task.setEndDate(new Date());
                System.out.println("End task " + task.getId().toString());
            } else {
                robot.doTask(task);
            }
        }
    }

    private Robot findRobotByTaskType(TaskType taskType) {
        return robots.stream()
                .filter(robot1 -> robot1.getTaskType().contains(taskType))
                .findAny()
                .orElse(null);
    }

    private Task findNewTask() {
        return tasks.stream()
                .filter(task -> task.getStatus().equals(TaskStatus.CREATED))
                .findFirst()
                .orElse(null);
    }

    private void initializeData() {
        tasks.add(new Task(TaskType.KILL_SOLDER));
        tasks.add(new Task(TaskType.KILL_SOLDER));
        tasks.add(new Task(TaskType.KILL_HEAVY));
        tasks.add(new Task(TaskType.KILLYOURSELF));

        robots.add(new RobotSolder(5, 10, "Калаш"));
        robots.add(new RobotSolder(12, 10, "Калаш"));
        Robot robotA = new RobotSolder(7, 10, "Калаш");
        robots.add(robotA);
    }

    public Task addTask(Task task) {
        tasks.add(task);
        return tasks.get(tasks.size());
    }

    public List<Task> getAllTasks() {
        return tasks;
    }

    public List<Robot> getAllRobots() {
        return robots;
    }
}
