package com.jazzteam.martynchyk.entity.robots;

import com.jazzteam.martynchyk.entity.task.Task;
import com.jazzteam.martynchyk.entity.task.TaskType;
import lombok.Getter;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Getter
public abstract class Robot {
    private UUID id;
    private double damage;
    private float speed;
    private Set<TaskType> taskType;
    protected final static float damageMultiple = 0.25f;

    public Robot(double damage, float speed, Set<TaskType> taskType) {
        this.id = UUID.randomUUID();
        this.damage = damage;
        this.speed = speed;
        this.taskType = taskType;
    }

    public abstract void doTask(Task task);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Robot robot = (Robot) o;
        return Double.compare(robot.damage, damage) == 0 &&
                Float.compare(robot.speed, speed) == 0 &&
                Objects.equals(id, robot.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, damage, speed);
    }

    @Override
    public String toString() {
        return "Robot{" +
                "id=" + id +
                ", damage=" + damage +
                ", speed=" + speed +
                '}';
    }
}
