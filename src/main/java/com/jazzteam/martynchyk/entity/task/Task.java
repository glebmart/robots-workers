package com.jazzteam.martynchyk.entity.task;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Task {
    private UUID id;
    private TaskType type;
    private TaskStatus status;
    private Date startDate;
    private Date endDate;

    public Task(TaskType type) {
        this.type = type;
        this.status = TaskStatus.CREATED;
        this.id=UUID.randomUUID();
        this.startDate=new Date();
    }
}
